import { BASE_URL, BEARER_TOKEN } from "@/constants.js"

export async function getCategories() {
  const response = await fetch(`${BASE_URL}/distributor/categories`, {
    headers: {
      Authorization: `Bearer ${BEARER_TOKEN}`,
    },
  })
  
  const data = await response.json()
  return data
}
