export const mock = [
  {
    id: 124,
    name: "Bar and Kitchenware",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 126,
        name: "Glassware",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 111,
    name: "Beer, Cider & RTD",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 123,
        name: "Beer - Other",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 37,
        name: "Beverage - Mixers",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 113,
        name: "Cider",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 49,
        name: "India Pale Ale",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/hbpmpp5o4ttkogw3eljt"
      },
      {
        id: 89,
        name: "Lager",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 117,
        name: "Larger",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 95,
        name: "Other Beer",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/sdwvy7fl9mxw2achysyv"
      },
      {
        id: 112,
        name: "Pale Ale",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 115,
        name: "Pilsner",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 114,
        name: "Ready to Drink",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 62,
    name: "Beverages",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 50,
        name: "Juices",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/cuiun5ckzphfh4mkgfin"
      },
      {
        id: 88,
        name: "Mixers & Sodas",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/eyx699rx4pwsbntbtvqf"
      },
      {
        id: 74,
        name: "Other Drinks",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/esyvvwoyziybuqnjjqvg"
      },
      {
        id: 136,
        name: "Tea, Coffee, Hot Chocolate",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/xkmwnyym1wbepmepdvyi"
      },
      {
        id: 51,
        name: "Water Still & Sparkling",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/i7bcymx3coxzmc4tcioj"
      }
    ]
  },
  {
    id: 121,
    name: "Cleaning Supplies",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: []
  },
  {
    id: 99,
    name: "Cognac",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: []
  },
  {
    id: 119,
    name: "Dry Pantry",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: []
  },
  {
    id: 122,
    name: "Fruit & Vegetables",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 54,
        name: "Herbs",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/dprfxfbzrauyjnvus6co"
      },
      {
        id: 52,
        name: "Vegetables",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/q7qpmsxpppdzu9ktklr0"
      }
    ]
  },
  {
    id: 120,
    name: "Grains & Pasta",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 135,
        name: "Bread",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 133,
        name: "Pasta",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 134,
        name: "Rice",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 63,
    name: "Meat & Seafood",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 45,
        name: "Beef",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/weyafds5tl9atui9oz43"
      },
      {
        id: 48,
        name: "Chicken & Poultry",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/uec2zvyzyf5y6hx0fiqw"
      },
      {
        id: 44,
        name: "Fish",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/lbkcuwmzufjznv7hock8"
      },
      {
        id: 137,
        name: "Frozen Protein",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/xa37nihfcpjbtmdecyyk"
      },
      {
        id: 46,
        name: "Lamb",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/rxlnj6j8yzgck5ilbovy"
      },
      {
        id: 129,
        name: "Meat - Other",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/qb4ttiusdgtbld7vhwen"
      },
      {
        id: 47,
        name: "Pork",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/qd5dgxyfnchlbbb1hdwc"
      },
      {
        id: 73,
        name: "Seafood - Other",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/hpn0s77cbeoljsi65jmr"
      },
      {
        id: 64,
        name: "Shellfish",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/rvrvh5c3zlmpemjrctkc"
      }
    ]
  },
  {
    id: 118,
    name: "Milk, Dairy, Eggs",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 131,
        name: "Cheese",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 132,
        name: "Eggs",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 130,
        name: "Milk",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 82,
    name: "Other",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 83,
        name: "Fruits",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 127,
    name: "Professional",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: []
  },
  {
    id: 106,
    name: "Spiritis",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 86,
        name: "Bitters",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 91,
        name: "Brandy, Cognac, and Eau di Vie",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 57,
        name: "Coffee, Tea, Hot Chocolate",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 107,
        name: "Gin",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 27,
        name: "Liqueurs",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 36,
        name: "Malt Whiskey - Japanese",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/a1byp7mc7poj9pjyx45i"
      },
      {
        id: 92,
        name: "Port, Vermouth and Sherry",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 77,
        name: "Rum",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 93,
        name: "Tequila & Mezcal",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 84,
        name: "Vodka",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 31,
        name: "Whiskey - Bourbon",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/dzlximvzo3vrdymsbkon"
      },
      {
        id: 56,
        name: "Whiskey - Single Malt",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/jh59djxyogtguknvmqgy"
      },
      {
        id: 85,
        name: "Whisky - Other",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 138,
    name: "Uncategorised",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 139,
        name: "Misc",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  },
  {
    id: 100,
    name: "Wine",
    image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category",
    children: [
      {
        id: 110,
        name: "Champagne & Sparkling",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 105,
        name: "Dessert",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 104,
        name: "Orange",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 101,
        name: "Red",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 103,
        name: "Rose",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      },
      {
        id: 102,
        name: "White",
        image: "https://res.cloudinary.com/orderez/image/upload/v1/categories/default-category"
      }
    ]
  }
];
